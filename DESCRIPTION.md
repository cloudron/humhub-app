## Overview

HumHub is a free and open-source social network software written on top of the Yii PHP framework that
provides an easy to use toolkit for creating and launching your own social network.

The platform can be used for internal communication and collaboration that can range from a few users
up to huge Intranets that serve companies with hundreds and thousands of employees.
The platform was meant to be self-hosted and currently comes with pretty normal requirements, working with most shared hosting environments around.
HumHub also supports themes and modules to extend the functionality for almost all requirements.
