<?php
/**
 * This file provides to overwrite the default HumHub / Yii configuration by your local common (Console and Web) environments
 * @see http://www.yiiframework.com/doc-2.0/guide-concept-configurations.html
 * @see http://docs.humhub.org/admin-installation-configuration.html
 * @see http://docs.humhub.org/dev-environment.html
 */

return [
    'components' => [
        'db' => [
            'dsn' => 'mysql:host=' . getenv('CLOUDRON_MYSQL_HOST') . ';dbname=' . getenv('CLOUDRON_MYSQL_DATABASE'),
            'username' => getenv('CLOUDRON_MYSQL_USERNAME'),
            'password' => getenv('CLOUDRON_MYSQL_PASSWORD'),
        ],
        'urlManager' => [
            'showScriptName' => false,
            'enablePrettyUrl' => true,
        ]
    ]
];
