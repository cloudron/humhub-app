#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 30000;

    let browser, app;
    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    const admin_username = 'admin';
    const admin_password = 'changeme';

    const postMessage = 'My test message!';
    const spaceName = 'cloudrontestspace';

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function visible(selector) {
        await browser.wait(until.elementLocated(selector), TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(selector)), TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    async function login(username, password, closePopup) {
        await browser.manage().deleteAllCookies();

        await browser.get(`https://${app.fqdn}/user/auth/login`);
        await browser.sleep(2000);

        await browser.wait(until.elementLocated(By.xpath('//input[@id="login_username"]')), TIMEOUT);
        await browser.findElement(By.xpath('//input[@id="login_username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@id="login_password"]')).sendKeys(password);
        await browser.sleep(2000);

        await browser.findElement(By.id('login-button')).click();
        await browser.sleep(2000);

        await browser.wait(until.elementLocated(By.xpath('//b[contains(text(), "Your dashboard is empty!")] | //div[contains(@class,"wall_humhubmodules")]')), TIMEOUT);

        if (closePopup) {
            console.log('closing popup');
            await browser.wait(until.elementLocated(By.xpath('//button[contains(text(), "Save")]'), TIMEOUT));
            await browser.findElement(By.xpath('//button[contains(text(), "Save")]')).click();
            await browser.sleep(3000);
        }
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);

        await browser.wait(until.elementLocated(By.id('account-dropdown-link')), TIMEOUT);
        await browser.findElement(By.id('account-dropdown-link')).click();
        await browser.sleep(5000);

        await browser.wait(until.elementLocated(By.xpath('//a[@data-menu-id="account-logout"]')), TIMEOUT);
        await browser.findElement(By.xpath('//a[@data-menu-id="account-logout"]')).click();
        await browser.sleep(5000);

        await browser.wait(until.elementLocated(By.xpath('//input[@id="login_username"]')), TIMEOUT);
    }

    async function getDashboard() {
        await browser.get(`https://${app.fqdn}/dashboard`);
        await browser.wait(until.elementLocated(By.xpath('//b[contains(text(), "Your dashboard is empty!")] | //div[contains(@class,"wall_humhubmodules")]')), TIMEOUT);
    }

    async function createSpace() {
        await browser.get(`https://${app.fqdn}/dashboard`);
        await browser.sleep(10000);

        await browser.findElement(By.xpath('//a[@id="space-menu"]/ancestor::li')).click();
        await browser.sleep(3000);

        await browser.findElement(By.xpath('//a[contains(text(), "Create Space")]')).click();
        await browser.sleep(10000);

        await browser.findElement(By.id('space-name')).sendKeys(spaceName);
        await browser.findElement(By.xpath('//input[@id="space-name"]/ancestor::form//button[contains(text(), "Next")]')).click();
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//a[contains(text(), "Done")]')).click();
        await browser.sleep(2000);
    }

    async function checkSpaceMembers() {
        await browser.get(`https://${app.fqdn}/s/${spaceName}`);
        await browser.sleep(3000);
        await browser.findElement(By.xpath(`//a[@data-action-url="/s/${spaceName}/space/membership/members-list"]/div`)).click();
        await visible(By.id('userlist-content'));
    }

    async function createPost() {
        await browser.get(`https://${app.fqdn}/s/${spaceName}`);
        await browser.findElement(By.xpath('//div[@id="contentForm_message"]/div/div[contains(@class, "humhub-ui-richtext")]')).click();
        await browser.findElement(By.xpath('//div[@id="contentForm_message"]/div/div[contains(@class, "humhub-ui-richtext")]')).sendKeys(postMessage);
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[@id="post_submit_button"]')).click();
        await browser.sleep(2000);
    }

    async function checkPost() {
        await browser.get(`https://${app.fqdn}/dashboard`);
        await browser.wait(until.elementLocated(By.xpath('//p[contains(text(), "' + postMessage + '")]')), TIMEOUT);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('wait for app to come up', async function () {
        console.log('waiting for app to come up');
        await browser.sleep(30000);
    });

    it('can ldap login', login.bind(null, username, password, false));
    it('can get Dashboard', getDashboard);
    it('can ldap logout', logout);

    it('can admin login', login.bind(null, admin_username, admin_password, false));

    it('can get Dashboard', getDashboard);

    it('can create a new space', createSpace);
    it('can create a new post', createPost);
    it('check Post', checkPost);
//    it('check members of space', checkSpaceMembers);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });
    it('check Post', checkPost);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', async function () {
        await browser.get('about:blank');
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('check Post', checkPost);
    it('can admin logout', logout);

    it('can ldap login', login.bind(null, username, password, false));
    it('can get Dashboard', getDashboard);
    it('can ldap logout', logout);

    it('move to different location', async function () {
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        console.log('Waiting for app to come up');
        await browser.sleep(30000);
    });
    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, admin_username, admin_password, false));
    it('can get Dashboard', getDashboard);

    it('check Post', checkPost);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    it('install app (no sso)', function () { execSync(`cloudron install --no-sso --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('wait for app to come up', async function () {
        console.log('Waiting for app to come up');
        await browser.sleep(30000);
    });
    it('can admin login', login.bind(null, admin_username, admin_password, false));
    it('can get Dashboard', getDashboard);
    it('can create a new space', createSpace);
    it('can create a new post', createPost);
    it('check Post', checkPost);
    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app for update', async function () {
        execSync(`cloudron install --appstore-id com.humhub.cloudronapp --location ${LOCATION}`, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);
    it('wait for app to come up', async function () {
        console.log('Waiting for app to come up');
        await browser.sleep(30000);
    });

    it('can ldap login', login.bind(null, username, password, false));
    it('can get Dashboard', getDashboard);
    it('can ldap logout', logout);

    it('can admin login', login.bind(null, admin_username, admin_password, false /* closePopup */));
    it('can get Dashboard', getDashboard);
    it('can create a new space', createSpace);
    it('can create a new post', createPost);
    it('check Post', checkPost);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });
    it('wait for app to come up', async function () {
        console.log('Waiting for app to come up');
        await browser.sleep(30000);
    });

    it('check Post', checkPost);
//    it('check members of space', checkSpaceMembers);

    it('can ldap login', login.bind(null, username, password, false));
    it('can get Dashboard', getDashboard);
    it('can ldap logout', logout);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
