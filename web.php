<?php

// file snippet from https://docs.humhub.org/docs/admin/reverse-proxy/#httpsssl-detection

// Enfore HTTPS link generation
$_SERVER['HTTPS'] = 'on';

return [
    // Further configuration options
];
