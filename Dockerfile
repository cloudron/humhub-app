FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# renovate: datasource=github-releases depName=humhub/humhub versioning=semver extractVersion=^v(?<version>.+)$
ARG HUMHUB_VERSION=1.17.1

# https://download.humhub.com/
# https://docs.humhub.org/docs/admin/installation/#enable-production-mode
RUN curl -L https://download.humhub.com/downloads/install/humhub-${HUMHUB_VERSION}.tar.gz | tar zx --strip-components 1 -C /app/code && \
    mv /app/code/.htaccess.dist /app/code/.htaccess && \
    sed -i "/YII_DEBUG/d" ./index.php && \
    sed -i "/YII_ENV/d" ./index.php && \
    chown -R www-data:www-data /app/code

# https://docs.humhub.org/docs/admin/installation/#file-permissions
RUN rm -rf /app/code/assets && ln -s /app/data/assets /app/code/assets && \
    rm -rf /app/code/protected/modules && ln -s /app/data/modules /app/code/protected/modules && \
    rm -rf /app/code/protected/runtime && ln -s /app/data/runtime /app/code/protected/runtime && \
    mv /app/code/uploads /app/code/uploads.orig && rm -rf /app/code/uploads && ln -sf /app/data/uploads /app/code/uploads && \
    mv /app/code/protected/config /app/code/protected/config.orig && ln -s /app/data/config /app/code/protected/config && \
    mv /app/code/themes /app/code/themes.orig && ln -s /app/data/themes /app/code/themes

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/humhub.conf /etc/apache2/sites-enabled/humhub.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf
RUN a2enmod rewrite headers rewrite expires cache

# configure mod_php
RUN crudini --set /etc/php/8.3/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP memory_limit 128M && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.save_path /run/app/sessions && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.gc_divisor 100

RUN ln -s /app/data/php.ini /etc/php/8.3/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.3/cli/conf.d/99-cloudron.ini

COPY start.sh common.php web.php /app/pkg/

CMD [ "/app/pkg/start.sh" ]
