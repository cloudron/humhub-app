#!/bin/bash

set -eu

yii='gosu www-data:www-data /usr/bin/php /app/code/protected/yii'
curl='curl --fail -sS -o /dev/null'

setup() {
    while [[ ! -f "/var/run/apache2/apache2.pid" ]]; do
        echo "==> Waiting for apache2 to start"
        sleep 1
    done

    echo "==> Setting up database"
    yes Y | $yii migrate/up --includeModuleMigrations=1

    echo "==> Setup name and url"
    $yii settings/set 'base' 'name' 'Humhub'
    $yii settings/set 'base' 'baseUrl' "${CLOUDRON_APP_ORIGIN}"

    echo "==> Init security based on use case"
    $yii settings/set 'base' 'useCase' 'intranet' # USECASE_SOCIAL_INTRANET in code
    $curl -X POST -H 'Content-Type: application/x-www-form-urlencoded' --data-raw 'SecurityForm%5BinternalAllowAnonymousRegistration%5D=0&SecurityForm%5BinternalRequireApprovalAfterRegistration%5D=0&SecurityForm%5BallowGuestAccess%5D=0&SecurityForm%5BcanInviteExternalUsersByEmail%5D=0&SecurityForm%5BenableFriendshipModule%5D=0' http://localhost:8000/installer/config/security
    $curl http://localhost:8000/installer/config/modules

    echo "==> Create admin user"
    $yii user/create admin admin@cloudron.local HumHub Admin # username email firstName lastName
    $yii user/set-password admin changeme
    $yii user/make-admin admin

    echo "==> Load sample data"
    _csrf=$(curl http://localhost:8000/installer/config/sample-data | grep '_csrf" value="' | sed -e 's,.*_csrf" value=",,' | sed -e 's,==">,==,')
    $curl -d "SampleDataForm[sampleData]=0&_csrf=${_csrf}" http://localhost:8000/installer/config/sample-data

    echo "==> Finish configuration"
    $curl http://localhost:8000/installer/config/finish
    $curl http://localhost:8000/installer/config/finished
}

upgrade() {
    echo "==> Running upgrade"

    # Flush caches
    $yii cache/flush-all

    # Database migration
    yes Y | $yii migrate/up --includeModuleMigrations=1
}

symlink_themes() {
    echo "==> Symlinking built-in themes"
    for theme in `find "/app/code/themes.orig"/* -maxdepth 0 -type d -printf "%f\n"`; do
        ln -sf "/app/code/themes.orig/${theme}" "/app/data/themes/${theme}" || true
    done
}

mkdir -p /app/data/{config,modules,assets,uploads,runtime,themes}

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

if [ -z "$(ls -A /app/data/config)" ]; then
    echo "==> First run"
    cp -r /app/code/protected/config.orig/* /app/data/config
    cp -r /app/code/uploads.orig/* /app/data/uploads
    cp /app/pkg/common.php /app/data/config/common.php
    symlink_themes
    chown -R www-data:www-data /app/data

    echo "==> Starting apache for setup"
    APACHE_CONFDIR="" source /etc/apache2/envvars
    rm -f "${APACHE_PID_FILE}"
    ( /usr/sbin/apache2 -DFOREGROUND ) &
    apache_pid=$!
    setup
    echo "==> Setup complete"
    kill ${apache_pid}
else
    symlink_themes
    chown -R www-data:www-data /app/data
fi

if ! upgrade; then
    echo "==> Could not upgrade"
    exit 1
fi

echo "==> Overwriting web.php"
cp /app/pkg/web.php /app/data/config/web.php

echo "==> Configuration"
$yii settings/set 'base' 'baseUrl' "${CLOUDRON_APP_ORIGIN}"
$yii settings/set 'base' 'mailer.systemEmailAddress' "${CLOUDRON_MAIL_FROM}"
$yii settings/set 'base' 'mailer.systemEmailName' "${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-HumHub}"
$yii settings/set 'base' 'mailer.transportType' "smtp"
$yii settings/set 'base' 'mailer.hostname' "${CLOUDRON_MAIL_SMTP_SERVER}"
$yii settings/set 'base' 'mailer.port' "${CLOUDRON_MAIL_SMTP_PORT}"
$yii settings/set 'base' 'mailer.username' "${CLOUDRON_MAIL_SMTP_USERNAME}"
$yii settings/set 'base' 'mailer.password' "${CLOUDRON_MAIL_SMTP_PASSWORD}"

if [[ -n "${CLOUDRON_LDAP_SERVER:-}" ]]; then
    # setting LDAP config up
    $yii settings/set 'ldap' 'enabled' "1"
    $yii settings/set 'ldap' 'hostname' "${CLOUDRON_LDAP_SERVER}"
    $yii settings/set 'ldap' 'port' "${CLOUDRON_LDAP_PORT}"
    $yii settings/set 'ldap' 'username' "${CLOUDRON_LDAP_BIND_DN}"
    $yii settings/set 'ldap' 'password' "${CLOUDRON_LDAP_BIND_PASSWORD}"
    $yii settings/set 'ldap' 'baseDn' "${CLOUDRON_LDAP_USERS_BASE_DN}"
    $yii settings/set 'ldap' 'loginFilter' "(username=%s)"
    $yii settings/set 'ldap' 'userFilter' '(&(objectclass=user))'
    $yii settings/set 'ldap' 'usernameAttribute' "username"
    $yii settings/set 'ldap' 'emailAttribute' "mail"
    $yii settings/set 'ldap' 'idAttribute' "username"
    $yii settings/set 'ldap' 'refreshUsers' "1"
fi

# do cron run immediately to hide the warning in web ui
gosu www-data:www-data /usr/bin/php /app/code/protected/yii cron/run &

echo "==> Starting HumHub"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND

